import scala.io.Source
import scala.annotation.tailrec

object Day1 extends App {

    val input = Source.fromResource("day1.txt").getLines()

    val ints = input.map(_.toInt).toList

    def massToFuel(mass: Int): Int =
        mass / 3 - 2

    val result = ints.fold(0)(_ + massToFuel(_))

    println(result)
    // 3420719

    // part2

    println("Part 2 ")
    def fuelForFuel(mass: Int): Int = {

        @tailrec
        def loop(m: Int, acc: Int): Int = {
            val extraMassRequired = massToFuel(m)
            if (extraMassRequired > 0) {
                loop(extraMassRequired, acc + extraMassRequired)
            } else {
                acc
            }
        }
        loop(massToFuel(mass), massToFuel(mass))
    }

    val result2 = ints.map(fuelForFuel).sum

    println(result2)

}

