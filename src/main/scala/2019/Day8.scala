import scala.io.Source
object Day8 extends App {

    val input: String = Source.fromResource("day8.txt").getLines().toList.head

    val dimensions = 25 * 6
    val grouped = input
    .grouped(dimensions)
    .toList

    val leastZeroes = grouped
    .sortBy(l => l.count(number => number == '0'))
    .head

    println(leastZeroes)

    val ones = leastZeroes.count(_ == '1')
    val twos = leastZeroes.count(_ == '2')

    println(ones * twos)

    val result2 = (0 to dimensions - 1).map(i => {
        val chars = grouped.map(layer => layer(i))
        chars.dropWhile(_ == '2').head
    })

    println(result2)


    println(    result2.grouped(25).mkString("\n")
    )
}