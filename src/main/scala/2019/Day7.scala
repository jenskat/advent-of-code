import scala.io.Source
import scala.annotation.tailrec

object Day7 extends App {

    val input: Iterator[String] = Source.fromResource("day7.txt").getLines()
    val opcodes = input.next().split(",").map(_.toInt).toIndexedSeq

    val Add = 1
    val Multiply = 2
    val Halt = 99
    val Input = 3
    val Output = 4

    val JumpTrue = 5
    val JumpFalse = 6
    val LessThan = 7
    val Equals = 8

    val PositionMode = 0
    val ImmediateMode = 1

    val padding = "0000"

    val ProvideStart = 5
    case class OutputException(i: Int) extends Exception
    def parse2(readFrom: Int = 0, input: IndexedSeq[Int], start: Int = ProvideStart): IndexedSeq[Int] = {
    //    println(input.take(20))
        val modeParsed = (padding + input(readFrom).toString()).takeRight(5).toSeq.map(_.toString.toInt)

        // println(s"modeparsed: $modeParsed")
        val opcode: Int = modeParsed.takeRight(2).mkString.toInt
        // println(s"opcode: $opcode")

        val amount = readAhead(opcode)

        val Jump = amount + 1

        val offset = readFrom + 1

        // println(s"offset : $offset")
        val vec = input.slice(offset, offset + amount)

        // println(s"vec $vec")
        val actionAndValue = modeParsed.dropRight(2).reverse.take(amount)

        if (opcode == Output) {
            val result = select(actionAndValue(0), offset, input)
            println("OUTPUT " + result)
            if (result == 0) {
                parse2(readFrom+Jump, input)
            } else {
                throw OutputException(result)
            }
        }
        if (opcode == JumpTrue) {
            val check = select(actionAndValue(0), offset, input)
            if (check != 0) {
                val pointer = select(actionAndValue(1), offset + 1, input)
                parse2(pointer, input)
            } else {
                parse2(readFrom+Jump, input)
            }

        }

        if (opcode == JumpFalse) {

            if (select(actionAndValue(0), offset, input) == 0) {
                val pointer = select(actionAndValue(1), offset + 1, input)
                parse2(pointer, input)
            } else
            {
                parse2(readFrom+Jump, input)
            }

        }


        val result = opcode match {
            case `Add` =>
                select(actionAndValue(0), offset, input) + select(actionAndValue(1), offset+1, input)
            case `Multiply` =>
                select(actionAndValue(0), offset, input) * select(actionAndValue(1), offset+1, input)
            case `Halt` => return input
            case `Input` => start
            case `LessThan` => if (select(actionAndValue(0), offset, input) < select(actionAndValue(1), offset+1, input)) 1 else 0
            case `Equals` => if (select(actionAndValue(0), offset, input) == select(actionAndValue(1), offset+1, input)) 1 else 0
        }

        // always in position mode
        val updated = input.updated(vec.last, result)

        parse2(readFrom+Jump, updated)

        // OUTPUT 9006673


    }

    for {
        a <- 1 to 5
        b <- 1 to 5
        c <- 1 to 5
        d <- 1 to 5
        e <- 1 to 5
    } yield parse2(input = opcodes, start = ???)


    def readAhead(opcode: Int): Int = if (opcode == 3 || opcode == 4) 1
    else if (opcode == JumpTrue || opcode == JumpFalse) 2
    else 3


    def select(mode: Int, index: Int, input: IndexedSeq[Int]) =
        if (mode == PositionMode) positionMode(index, input)
        else immediateMode(index, input)

    def positionMode(i: Int, input: IndexedSeq[Int]): Int =
        input(input(i))

    def immediateMode(i: Int, input: IndexedSeq[Int]): Int =
        input(i)


   // val updatedOpCodes = opcodes.updated(1, 12).updated(2, 2)
    val result = parse2(input = opcodes)

}