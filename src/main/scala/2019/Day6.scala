import scala.io.Source
object Day6 extends App {

    val input: List[String] = Source.fromResource("day6.txt").getLines().toList.sorted


    val direct = input.length

    val Orbit = ')'

    val splitted = input.map(_.split(Orbit).toList)

    val dag: Map[Node, List[Node]] = Map.empty
    val filledDag = splitted.foldLeft(dag)((d, list) => {
        val List(a, b) = list
        val nodes = d.getOrElse(Node(a), List())
        d.updated(Node(a), nodes :+ Node(b))
    }
    )

    def countDirect(in: Map[Node, List[Node]]): Int =
        startFrom(Node("COM"), in).size

    def countIndirect(in: Map[Node, List[Node]]): Int =
        startFromIndirect(Node("COM"), in)

    // splitted.zip(splitted.tail).map{ case (List(a, b), List(c, d)) =>

    // }
    // val nodes = for {
    //     line <- input
    //     List(parent, child) = line.split(Orbit).toList
    // } yield {
    //     val c = Node(child)
    //     val p = Node(parent, List(c))
    //     (p, c)
    // }

    println(filledDag)


    def startFrom(node: Node, in: Map[Node, List[Node]]): Set[Node] = {
        def loop(node: Node, dag: Map[Node, List[Node]], acc: Set[Node]): Set[Node] = {
            dag.get(node) match {
                case Some(n) => n.toSet.flatMap((nn: Node) => loop(nn, dag, acc + nn))
                case None => acc
            }

        }
        loop(node, in, (Set.empty))
    }

    def startFromIndirect(node: Node, in: Map[Node, List[Node]]): Int = {
        def loop(node: Node, acc: Int, depth: Int = 0): Int = {
            println(s"Accumulator is $acc")
            in.get(node) match {
                case Some(n) =>{
                    println(s"$n at depth $depth")
                    val t = n.map(nn => loop(nn, depth + acc, depth + 1))
                    println(t)
                    t.sum
                }
                case None => acc
            }

        }
        loop(node, 0)
    }


    // val direct = countDirect(filledDag)
    val indirect = countIndirect(filledDag)
    val result = direct + indirect
    println(s"Direct: $direct")
    println(s"InDirect: $indirect")

    println(s"Result: $result")

    case class Node(name: String) {
    }

}


object Problem06 extends App {
        var orbitmap = (Source.fromResource("day6.txt").getLines.toArray
                     .map(_.split("\\)")).foldLeft(Map[String,String]())
                       ((acc,a)=>acc+(a(1)->a(0))))+("COM"->"COM")
        def next(node:String) = orbitmap.filter(_._2==node).map(_._1)
        def neighbors(node:String) = orbitmap(node) :: next(node).toList
        def apply(part2: Boolean = false)  ={
          var done = false
          var idx = 1
          var visited : Set[String] = Set()
          var current = Set(if (part2) "YOU" else "COM")
          var result = 0;
          while (!done) {
            visited ++= current
            current = (current ++ current.flatMap(neighbors)).diff(visited)
            var size = current.size
            done = if (part2) current.contains("SAN") else size == 0
            result += idx*size
            idx += 1
          }
          if (part2) idx-3 else result
        }
        println(Problem06())
        println(Problem06(true))

  }