object Day4 extends App {
    val start = 178416
    val end = 676461

    val result = for {
        number <- start to end
        if (number.isMonotone && number.containsDouble)
    } yield number

    println(result.size)

    // 1650

    // part 2

    val result2 = for {
        number <- result
        if (number.containsDoubleStrict)
    } yield number

    println(result2.size)

    // 1129

    implicit class IntOps(i: Int) {
        def isMonotone: Boolean = {
            val sequence = i.toString()
            sequence.zip(sequence.tail).forall{ case (a, b) => a.toInt <= b.toInt }
        }

        def containsDouble: Boolean = {
            val sequence = i.toString()
            sequence.zip(sequence.tail).find{ case (a, b) => a == b }.nonEmpty
        }

        def containsDoubleStrict: Boolean = {
            val sequence = i.toString()
            val sameDigits = sequence.groupBy(identity)
            sameDigits.count{ case ((c, s)) => s.length() == 2} > 0
        }
    }

}