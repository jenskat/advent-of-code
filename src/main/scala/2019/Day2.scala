import scala.io.Source
import scala.annotation.tailrec
object Day2 extends App {

    val input: Iterator[String] = Source.fromResource("day2.txt").getLines()
    val opcodes = input.next().split(",").map(_.toInt).toIndexedSeq

    val Jump = 4
    val Add = 1
    val Multiply = 2
    val Halt = 99

    @tailrec
    def parse(actionPos: Int = 0, firstPos: Int = 1, secondPos: Int = 2, resultPos: Int = 3, input: IndexedSeq[Int]): IndexedSeq[Int] = {
        val result = input(actionPos) match {
            case `Add` =>
            input(input(firstPos)) + input(input(secondPos))
            case `Multiply` => input(input(firstPos)) * input(input(secondPos))
            case `Halt` => return input
        }

        val updated = input.updated(input(resultPos), result)

        parse(actionPos+Jump, firstPos+Jump, secondPos+Jump, resultPos+Jump, updated)
    }


    val updatedOpCodes = opcodes.updated(1, 12).updated(2, 2)
    val result = parse(input = updatedOpCodes)

    // println(parse(0,1,2,3, IndexedSeq(1,0,0,0,99)))
    //println(result)
    println(result(0))

    // 3654868


    // part 2

    val bruteForce = 19690720

    val result2 = for {
        noun <- 0 to 99
        verb <- 0 to 99
        updatedOpCodes = opcodes.updated(1, noun).updated(2, verb)
        if (parse(input = updatedOpCodes)(0) == bruteForce)
    } yield noun * 100 + verb

    println(result2)

}