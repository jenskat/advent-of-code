import scala.io.Source
import scala.annotation.tailrec

object Day3 extends App {

    val input = Source.fromResource("day3.txt").getLines()

    val wires = input.map(line => line.split(",").toList).toList

    val coords = wires.map(wire => wire.map(Direction(_)))

    val wire1 = coords(0)
    val wire2 = coords(1)



    val origin = Coord(0,0)

    def originToEnd(directions: List[Direction], start: Coord = origin): List[Coord] = {
        def loop(list: List[Direction], result: List[Coord]): List[Coord] = {
            list match {
                case Nil => result.reverse
                case head :: tl => loop(tl, result.head.toDirection(head) +: result)
            }
        }
        loop(directions, List(start))
    }


    def manhattanDistance(start: Coord, end: Coord): Int =
        Math.abs(start.x - end.x) + Math.abs(start.y - end.y)



    val coordsWire1 = originToEnd(wire1)
    // println(coordsWire1)
    val coordsWire2 = originToEnd(wire2)
    // println(coordsWire2)

    val lines1 = coordsWire1.zip(coordsWire1.tail).map(Line(_))
    val lines2 = coordsWire2.zip(coordsWire2.tail).map(Line(_))

    val intersections = for {
        line1 <- lines1
        line2 <- lines2
        coord = line1.crosses(line2)
        if coord.nonEmpty
    } yield coord.get

    val result = intersections.sortBy(c => manhattanDistance(origin, c))

  //  println(result)
    println(result.tail.head) // first elem is origin itself. drop that.

    println(manhattanDistance(origin, result.tail.head))

    // Coord(-4,-228): 232
    // part2

     val delays =   intersections.map(crossing => {
            val crossingLine1 = lines1.find(_.coords.contains(crossing)).get
            val indexOfCrossing1 = lines1.indexOf(crossingLine1)
            val lengthLine1 = lines1.take(indexOfCrossing1).foldLeft(0)( _ + _.length) + Line(crossingLine1.start, crossing).length

            val crossingLine2 = lines2.find(_.coords.contains(crossing)).get
            val indexOfCrossing2 = lines2.indexOf(crossingLine2)
            val lengthLine2 = lines2.take(indexOfCrossing2).foldLeft(0)( _ + _.length) + Line(crossingLine2.start, crossing).length

            lengthLine1 + lengthLine2
        })

        println(delays.sorted.tail.head)

        // 6084
    }

object Line {
    def apply(elems: (Coord, Coord)): Line = Line(elems._1, elems._2)
    def directionX(start: Coord, end: Coord): Int = if (end.x-start.x > 0) 1 else -1
    private def directionY(start: Coord, end: Coord): Int = if (end.y-start.y > 0) 1 else -1
}
case class Line(start: Coord, end: Coord) {
    import Line._
    val coords: Set[Coord] = (for {
        x <- start.x to end.x by directionX(start, end)
        y <- start.y to end.y by directionY(start, end)
    } yield Coord(x, y)).toSet

    def crosses(that: Line): Option[Coord] =
     this.coords.intersect(that.coords).headOption

    def length: Int =
      Math.abs(start.x - end.x) + Math.abs(start.y - end.y)

}

case class Coord(x: Int, y: Int) {
    def toDirection(d: Direction): Coord =
        d match {
            case Up(distance) => copy(y = y + distance)
            case Down(distance) => copy(y = y - distance)
            case Right(distance) => copy(x = x + distance)
            case Left(distance) => copy(x = x - distance)
        }
}

object Direction {
    def apply(in: String): Direction = in.toSeq match {
        case 'D' +: rest => Down(rest.toString.toInt)
        case 'U' +: rest => Up(rest.toString.toInt)
        case 'R' +: rest => Right(rest.toString.toInt)
        case 'L' +: rest => Left(rest.toString.toInt)
    }
}
sealed trait Direction {
    def distance: Int
}
case class Up(override val distance: Int) extends Direction
case class Down(override val distance: Int) extends Direction
case class Left(override val distance: Int) extends Direction
case class Right(override val distance: Int) extends Direction
