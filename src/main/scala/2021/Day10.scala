package `2021`

import scala.io.Source
import scala.util.{Try, Success, Failure}

object Day10:
  val input = Source.fromResource("2021/day10.txt").getLines().toList


  /**
   * 
    If a chunk opens with (, it must close with ).
    If a chunk opens with [, it must close with ].
    If a chunk opens with {, it must close with }.
    If a chunk opens with <, it must close with >.
*/
  val isOpening = Set('(','[' ,'{' ,'<')
  val isClosing = Set(')', ']', '}', '>')
  val mapping = isClosing.zip(isOpening).toMap
  val score = isClosing.zip(Set(3, 57, 1197, 25137)).toMap
  def findChar(in: List[Char]): Option[Char] = {
    def loop(remainder: List[Char], stack: List[Char]): Option[Char] =
      remainder match 
        case Seq() => None
        case head +: tail => 
          if (isClosing(head))
            if (mapping(head) == stack.head)
              loop(tail, stack.tail)
            else Option(head)

          else loop(tail, head +: stack)


    loop(in, List())
  }

  val reverseMapping = mapping.map(_.swap)
  def autocomplete(in: List[Char]): List[Char] = {
    def loop(remainder: List[Char], stack: List[Char], acc: List[Char]): List[Char] =
      remainder match
        case Seq() => acc
        case head +: tail => 
          if (isOpening(head))
            if (stack.nonEmpty && reverseMapping(head) == stack.head)
              loop(tail, stack.tail, acc)
            else loop(tail, stack, acc :+ reverseMapping(head))
          else loop(tail, head +: stack, acc)
    loop(in.reverse, List(), List())
  }

  val score2 = isClosing.zip(Set(1,2,3,4)).toMap

  val result = input.flatMap(string => findChar(string.toCharArray.toList)).map(c => score(c)).sum

  val result2 = input.filterNot(string => findChar(string.toCharArray.toList).isDefined)
    .map(line => {
      autocomplete(line.toCharArray.toList)
    })
    .map(autoc => autoc.foldLeft(0L)((acc, ch) => (acc * 5) + score2(ch) ))

  val l = result2.length

  val result2score = result2.sorted.apply(l/2)  

  @main
  def main10 =
    println(result)
    println(result2score)
