package `2021`

import scala.io.Source

object Day1:
  val input = Source.fromResource("2021/day1.txt").getLines()

  val ints = input
    .map(_.toInt)
    .toList

  val output = ints
    .sliding(2)
    .count{case List(a,b) => b > a}
  
  val output2 = ints
    .sliding(3)
    .map(_.sum)
    .sliding(2)
    .count{case Seq(a: Int,b: Int) => b > a}
  
  @main
  def main =
    println(output)
    println(output2)
