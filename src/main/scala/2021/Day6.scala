package `2021`

import scala.io.Source

object Day6:
  val input = Source.fromResource("2021/day6.txt").getLines().toList

  val fish:Map[Int, Long] = input.head.split(',').map(_.toInt).groupBy(identity).map((key, value) => key -> value.length.toLong)

  def loop(in: Map[Int, Long]): Map[Int, Long] = 
    val promote = in.get(0).getOrElse(0L)
    val next = (in - 0).map((key, value) => (key - 1)-> value) + (8 -> promote)
    val six = next.get(6).getOrElse(0L)
    next + (6 -> (six + promote))
 

  val result = (1 to 256)
    .foldLeft(fish){ case (acc, _) => loop(acc)}.values.sum

  @main
  def main6 =
    println(result) 
    // println(result2)
