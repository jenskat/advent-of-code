package `2021`

import scala.io.Source

object Day12:
  val input = Source.fromResource("2021/day12.txt").getLines().toList

  val paths : Map[String, List[String]]= input.map(_.split('-'))
  .flatMap(arr => List((arr(0), arr(1)), (arr(1), arr(0)))).foldLeft(Map[String, List[String]]())((acc, entry) => {
    acc.get(entry._1) match 
      case Some(vals) => acc + (entry._1 -> (vals :+ entry._2))
      case None => acc + (entry._1 -> List(entry._2))
  })

  def solve(in: Map[String, List[String]]): Int =
    def loop(current: String, in: Map[String, List[String]], lowerCaseVisited: Set[String]): Int = 
      if (current == "end") 1
      else {
        in.get(current) match {
          case Some(nodes) => nodes.map(node => {
            if (current.toLowerCase == current) {
              if (lowerCaseVisited(current)) 0
              else loop(node, in, lowerCaseVisited+current)
            }
            else loop(node, in, lowerCaseVisited)
          }
          ).sum
          case None => 0
        }
      }

    loop("start", in, Set())


  def solve2(in: Map[String, List[String]]): Int = {
    def loop(current: String, in: Map[String, List[String]], lowerCaseVisited: Set[String], twice: Set[String]): Int = {
      if (current == "end") 1
      else {
        in.get(current) match { // options:
          case Some(nodes) =>
            nodes.filterNot(_ == "start").map(node => {
              if (current.toLowerCase == current) {
                if (lowerCaseVisited(current) && twice.isEmpty) loop(node, in, lowerCaseVisited, twice+current) // visited once, we can continue
                else if (lowerCaseVisited(current) && twice.nonEmpty) 0
                else loop(node, in, lowerCaseVisited+current, twice) // never seen before, add it to our list
              }
              else loop(node, in, lowerCaseVisited, twice) // not lowercase we can visit this.
          }
          ).sum
          case None => 0
        }
      }
    
    }

    loop("start", in, Set(), Set())

    }

  val result = solve(paths)
  val result2 = solve2(paths)

  @main
  def main12 =
    println(result)
    println(result2)
