package `2021`

import scala.io.Source

object Day8:
  val input = Source.fromResource("2021/day8.txt").getLines().toList

  val parsed = input.map(_.split('|').toList).map ( inout => 
    val in = inout(0).split(" ").map(_.strip).map(_.sortBy(identity)).toList
    val out = inout(1).split(" ").filter(!_.isBlank).map(_.sortBy(identity)).toList
    (in -> out)
  )

  val lookingFor = Set(2,3,4,7)

  val result = parsed.map(_._2.count(s => lookingFor(s.length))).sum

  val mapping: Map[Int, Set[Char]] = Map(
    1 -> Set(          'c',           'f'), // 2
    4 -> Set(     'b', 'c', 'd',      'f'), // 4
    7 -> Set('a',      'c',           'f'), // 3
    8 -> Set('a', 'b', 'c', 'd', 'e', 'f', 'g'), // 7
    3 -> Set('a',      'c', 'd',      'f', 'g'), // 5 = 7 || 4 - 1 || 1
    2 -> Set('a',      'c', 'd', 'e',      'g'), // 5 = 7 - 1 || 4 - 1 || 1 - 1 
    5 -> Set('a', 'b',      'd',      'f', 'g'), // 5 = 7 - 1 || 4 - 1 || 1 - 1 
    6 -> Set('a', 'b',      'd', 'e', 'f', 'g'), // 6 = 
    0 -> Set('a', 'b', 'c',      'e', 'f', 'g'), // 6
    9 -> Set('a', 'b', 'c', 'd',      'f', 'g'), // 6
  )

  val mapping2 = mapping.map(_.swap)
  
  

  def stringToCandidates(in: String): Set[Int] = 
    val split = in.toCharArray.toSet
    val potentialCandidates = mapping2.filter((key, value) => key.size == split.size)
    potentialCandidates.values.toSet

  def filterSet(in: Set[Char], other: Set[Char]): Set[Char] =
    in diff other
  

  def solveMapping(in: List[String]): Map[String, Int] = {
    val stringLengths = in.map(_.sortBy(identity)).groupBy(identity).keys.toList
    println(stringLengths)
    
    val one: Set[Char] = stringLengths.filter(_.length == 2).headOption.map(_.toSet).getOrElse(Set())
    val seven = stringLengths.filter(_.length == 3).headOption.map(_.toSet).getOrElse(Set())
    val four = stringLengths.filter(_.length == 4).headOption.map(_.toSet).getOrElse(Set())
    val eight = stringLengths.filter(_.length == 7).headOption.map(_.toSet).getOrElse(Set())

    val sixZeroNine = stringLengths.filter(_.length == 6)
    val twoThreeFive = stringLengths.filter(_.length == 5)

    val three = if (one.nonEmpty) {
      twoThreeFive.find(number => filterSet(number.toSet, one).size == 3)
    } else if (seven.nonEmpty) {
      twoThreeFive.find(number => filterSet(number.toSet, seven).size == 2)
    } else None

    val five =  if (four.nonEmpty) {
     twoThreeFive.find(number => filterSet(number.toSet, four).size == 3)
    } else None

    val six = if (one.nonEmpty) {
      sixZeroNine.find(number => filterSet(number.toSet, one).size == 5)
    } else if (seven.nonEmpty) {
      sixZeroNine.find(number => filterSet(number.toSet, seven).size == 4)
    } else None

    val nine = if (four.nonEmpty) {
      sixZeroNine.find(number => filterSet(number.toSet, four).size == 2)
    } else None

    val two = twoThreeFive.filterNot(s => three.getOrElse("") == s || five.getOrElse("") == s)

    val zero = sixZeroNine.filterNot(s => nine.getOrElse("") == s || six.getOrElse("") == s)

    println("ONE")
    println(one)
    println(two)
    println(three)
    println(four)
    println(five)
    println(six)
    println(seven)
    println(eight)
    println(nine)
    println(zero)


    val m = Map(
      one.mkString -> 1,
      two.mkString -> 5,
      three.mkString -> 3,
      four.mkString -> 4,
      five.mkString -> 2,
      six.mkString -> 6,
      seven.mkString -> 7,
      eight.mkString -> 8,
      nine.mkString -> 9,
      zero.mkString -> 0,
    ).map((key, value) => key.sortBy(identity) -> value)
    println(m)
    m
  }

  def transcribe(in: List[String], mapping: Map[String, Int]): Int = 
    Integer.parseInt(in.flatMap(mapping.get(_)).mkString)


  val result2 = parsed.map(entry => 
    transcribe(entry._2.map(_.sortBy(identity)), solveMapping(entry._1 ++ entry._2)))

  @main
  def main8 =
    // transcribe(parsed.head._2, solveMapping(parsed.head._1))
    println("MAIN8")


   // println(parsed) 
    println(result2)
    println(result2.sum)
