package `2021`

import scala.io.Source

object Day7:
  val input = Source.fromResource("2021/day7.txt").getLines().toList

  val crabs = input.head.split(',').map(_.toInt).toList

  val minPos = crabs.min
  val maxPos = crabs.max

  val positions = for {
    i <- (minPos to maxPos)

  } yield {
    crabs.foldLeft(0){ (acc, pos) => Math.abs(pos - i) + acc}
  }

  val result = positions.min

  val result2 = (minPos to maxPos).map(i => crabs.foldLeft(0){ (acc, pos) => (0 to Math.abs(pos - i)).sum + acc}).min

  @main
  def main7 =
    println(result) 
    println(result2) 
    // println(result2)
