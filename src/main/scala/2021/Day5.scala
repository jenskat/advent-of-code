package `2021`

import scala.io.Source

object Day5:
  val input = Source.fromResource("2021/day5.txt").getLines().toList

  type Point = (Int, Int)
  type Coord = (Point, Point)

  val ints : List[Coord] = input.map(s => {
    val coord = s.split(" -> ")
    point(coord(0)) -> point(coord(1))
  }).toList

  def createRange(a: Int, x: Int): Range =  if (a > x) {
      (a to x by -1)
      } else {
        (a to x)
      } 

  def fillLine(in: Coord): List[Point] = 
    in match {
    case ((a,b), (x,y)) if a == x => createRange(b, y).map((a,_)).toList
    case ((a,b), (x,y)) if b == y => createRange(a, x).map((_, b)).toList
    case ((a,b), (x,y)) if Math.abs(a-x) == Math.abs(b-y) => 
      val xrange = createRange(a,x)
      val yrange = createRange(b,y)
      xrange.zip(yrange).toList
    case _ => List()
  }

  def point(in: String): Point = 
    val s = in.split(',').map(_.toInt)
    (s(0), s(1))

  val result = ints
    .flatMap(fillLine(_))
    .groupBy(identity)
    .count(_._2.length > 1)

  @main
  def main5 =
    println(result) 
    // println(result2)
