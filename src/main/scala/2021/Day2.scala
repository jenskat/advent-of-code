package `2021`

import scala.io.Source

object Day2:
  val input = Source.fromResource("2021/day2.txt").getLines()

  val ints = input
    .map(_.split(' '))
    .map(arr => (arr(0) -> arr(1).toInt))
    .toList
    //.map(item => item.toInt)
    //.toList

  val result = ints.foldLeft((0,0)){case ((forward, down), item) => item._1 match {
    case "up" => forward -> (down - item._2)
    case "forward" => (forward + item._2 )-> down
    case "down" => forward -> (down + item._2)
  }}

  val result2 = ints.foldLeft((0L,0L,0L)){case ((forward, down, aim), item) => item._1 match {
    case "up" => (forward, down, aim - item._2)
    case "forward" => ((forward + item._2 ), (down + item._2 * aim),  aim)
    case "down" => (forward ,down , aim + item._2)
  }}


  @main
  def main2 =
    println(result)
    println(result._1 * result._2)
    println(result2)
    println(result2._1 * result2._2)
    // println(output2)
