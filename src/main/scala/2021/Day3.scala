package `2021`

import scala.io.Source

object Day3:
  val input = Source.fromResource("2021/day3.txt").getLines().toList

  val ints = input
  .map(_.toCharArray)
  .toList
  .transpose
  .map(list => list.partition(_ == '0'))

  val epsilon = ints
    .foldLeft(""){ case (binary, (zeros, ones)) => binary + (if (zeros.length > ones.length) "0" else "1") }
  val gamma = ints
    .foldLeft(""){ case (binary, (zeros, ones)) => binary + (if (zeros.length < ones.length) "0" else "1") }

  val length = input.take(1).length
  
  def select(index: Int, input: List[String], pred: (Int, Int) => Boolean): String =
    if (index > input.head.size || input.size == 1) 
      println("done") 
      input.head
    else 
      val (zeroes, ones) = input.partition(string => string(index) == '0')
      println(s"${zeroes.length} - ${ones.length}")
      val selector = if (pred(zeroes.length,ones.length))
          println(s"selected 0")
          '0'
      else     
        println(s"selected 1") 
        '1'
      val res = input.filter(string => string(index) == selector)
      select(index + 1, res, pred)
  
  val test = List(
    "00100",
"11110",
"10110",
"10111",
"10101",
"01111",
"00111",
"11100",
"10000",
"11001",
"00010",
"01010"
  )
  
  val oxygen = select(0, input, _ > _)
  val co2 = select(0, input, _ <= _)

  @main
  def main3 =
    // println(ints)
    println(Integer.valueOf(epsilon, 2) * Integer.valueOf(gamma, 2))
    // println(oxygen + " " + co2)
    println(Integer.valueOf(oxygen, 2) * Integer.valueOf(co2, 2))
