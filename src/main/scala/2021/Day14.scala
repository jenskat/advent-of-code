package `2021`

import scala.io.Source

object Day14:
  val input = Source.fromResource("2021/day14.txt").getLines().toList

  // puzzle start
  val polymer = input.head

  // Translation table what character to insert: "AA" -> "C"
  val mapping: Map[String, Char] = input.drop(2).map(l => {
    val subs = l.split(" -> ")
    subs(0) -> subs(1).charAt(0)
    }
  ).toMap

  // Polymer split up in Pairs and count how many pairs. 
  val polymers: Map[String, Long] = polymer.sliding(2).map(_.mkString).toList.groupBy(identity).map((k, v) => k -> v.size.toLong)

  def substitute2(in: Map[String, Long], subs: Map[String, Char], counter: Map[Char, Long]): (Map[String, Long], Map[Char, Long]) = {
    val makePairs = in.toList
        .flatMap((polymer, count) => List((polymer(0).toString + subs(polymer), count), (subs(polymer) + polymer(1).toString, count)))
        .groupBy(_._1).map((polymer, list) => polymer -> list.map(_._2).sum)
  // NCNBCHB NN|NC, CN, NC|NB, BC, CB|CH, HB
  // Count the characters. NN -> 1C , NC -> 1 B
    val countChars: Map[Char, Long] = in.toList
      .map((key, count) => (subs(key), count))
      .groupBy(_._1).map((char, groups) => char -> groups.map(_._2).sum)

    (makePairs, mergeMapChar(countChars, counter))

      // println(s"MatchedKEYS: $newKeys")

      // val mergedMap = newKeys.map(elem => {
      //   val x = elem match {
      //     case Seq(rem, sub, a, b) => updateMap2(rem, a, b, sub, in)
      //     case Seq(a) => in // no polymer created
      //   }
      //   x
      // }).fold(Map())((acc, m) => mergeMap(acc, m))
      // println(mergedMap)
      // (mergedMap, (indi.keySet ++ counter.keySet)
      //   .map(key => (key, indi.getOrElse(key, 0L) + counter.getOrElse(key, 0L)))
      //   .toMap)
  }

  def mergeMapChar(map1: Map[Char, Long], map2: Map[Char, Long]): Map[Char, Long] = 
    (map1.keySet ++ map2.keySet)
        .map(key => (key, map1.getOrElse(key, 0L) + map2.getOrElse(key, 0L)))
        .toMap

  def mergeMap(map1: Map[String, Long], map2: Map[String, Long]): Map[String, Long] = 
    (map1.keySet ++ map2.keySet)
        .map(key => (key, map1.getOrElse(key, 0L) + map2.getOrElse(key, 0L)))
        .toMap
  
  def updateMap2(current: String, left: String, right: String, in: Map[String, Long]): Map[String, Long] = 
    val currentPolymerCount = in.get(current).getOrElse(0L) // current amount of duos
    val newLeft = in.get(left).getOrElse(0L) + currentPolymerCount
    val newRight = in.get(right).getOrElse(0L) + currentPolymerCount
    val x=Map() + (left -> currentPolymerCount) + (right -> currentPolymerCount) 
    // println(x)
    x



  def substitute(in: String, replacements: Map[String, Char]): String = 
    in.sliding(2).foldLeft(""){ case (acc, iterator) => {
      
      val append: String = replacements.get(iterator.toList.mkString) match {
        case Some(rep: Char) => rep.toString
        case None => ""
      }
      acc + iterator.head + append
      }
    } + in.last

  

  val result: String = (1 to 10).foldLeft(polymer)((acc, _) => substitute(acc, mapping))

  val charCount = polymer.groupBy(identity).map((k, v) => k -> v.size.toLong) // Count BCHN -> Long
  val result2: (Map[String, Long], Map[Char, Long])= (1 to 40).foldLeft((polymers, charCount))((acc, _) => {
    val sub = substitute2(acc._1, mapping, acc._2)
    sub
  })

  def updateCharMap(elem: Char, value: Long, in: Map[Char, Long])  : Map[Char, Long] = 
    val current = in.get(elem).getOrElse(0L)
    in + (elem -> (current+value))
  


  val chars = result.toCharArray.groupBy(identity).map((c, arr) => c -> arr.size)
  val max = chars.maxBy(_._2)
  val min = chars.minBy(_._2)


  val max2 = result2._2.maxBy(_._2)
  val min2 = result2._2.minBy(_._2)

  @main
  def main14 =
    // println(result)

    println(max._2 - min._2)

    println(result2)

    println(max2._2 - min2._2)

