package `2021`

import scala.io.Source

object Day4:
  val input = Source.fromResource("2021/day4.txt").getLines().toList


  val ints = input.head.split(',').map(_.toInt).toList

  val boards: List[List[String]] = input.tail.filter{_.nonEmpty}.grouped(5).toList
  
  val parsed = boards.map(board(_)) 

  def parseLine(in: String): List[Int] = in.split(" ").flatMap(_.toIntOption).toList

  def board(in: List[String]): (List[List[Int]], List[List[Int]]) = 
    val rows : List[List[Int]]= in.map(parseLine(_)).filter(_.nonEmpty)
    rows -> rows.transpose
  
  def bingo(nr: Int, board: (List[List[Int]], List[List[Int]])): (Boolean, (List[List[Int]], List[List[Int]])) = 
    val (rows, columns) = board
    val updatedRows = rows.map(_.filter(_ != nr))
    val updatedColumns = columns.map(_.filter(_ != nr))
    val isBingo = updatedRows.find(_.isEmpty).isDefined || updatedColumns.find(_.isEmpty).isDefined
    (isBingo, (updatedRows, updatedColumns))

  def loop(boards: List[(List[List[Int]], List[List[Int]])], numbers: List[Int]): (Int, (List[List[Int]], List[List[Int]])) =
    numbers match
      case head +: tail => 
        val updatedBoards = boards.map(bingo(head, _))
        updatedBoards.find(_._1) match {
          case None => loop(updatedBoards.map(_._2), tail)
          case Some((_, bingoBoard)) => head -> bingoBoard
        }
      case _ => 0 -> boards.head
    

  val winner = loop(parsed, ints)

  val result = winner._1 * winner._2._1.map(_.sum).sum


  // part 2
  def loop2(boards: List[(List[List[Int]], List[List[Int]])], numbers: List[Int]): (Int, (List[List[Int]], List[List[Int]])) = {
    if (numbers.isEmpty) {
      0 -> boards.head
    }
    else {
      val updatedBoards = boards.map(board => bingo(numbers.head, board))
      
      val cards = updatedBoards.count(!_._1)
      val bingoBoard = updatedBoards.find(_._1)

      if (cards == 0 && bingoBoard.isDefined) {
        numbers.head -> bingoBoard.get._2
      }
      else {
        loop2(updatedBoards.filter(!_._1).map(_._2), numbers.tail)
      }
    }
  }

  val loser = loop2(parsed, ints)
  val result2 = loser._1 * loser._2._1.map(_.sum).sum

  @main
  def main4 =
    println(result)
    println(result2)
