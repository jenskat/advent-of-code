package `2021`

import scala.io.Source
import `2021`.Day11.Bat

object Day11:
  val input = Source.fromResource("2021/day11.txt").getLines().toList

  class Bat(var value: Int, val pos: (Int, Int), var peers: Set[Bat] = Set()) {
    def addPeer(l: Bat): Unit = 
      peers = peers + l

    var iteration: Int = 0

    var timesExploded = 0

    var isExploded = false
    def shouldExplode = value > 9

    def tick(): Unit = {
      iteration += 1
      isExploded = false
      increase()
    }

    def increase(): Unit = 
      if (!isExploded)
        value += 1

    def explode(): Unit = 
      if (shouldExplode && !isExploded)
        value = 0
        isExploded = true
        timesExploded += 1
        peers.foreach(_.increase())

  }

  val bats: List[List[Bat]] = input.zipWithIndex
    .map((line, row) => line.zipWithIndex//.tapEach(println)
    .map((c, column) => {
      Bat(c.toString.toInt, (row, column))
    }).toList)

  val xmax = input.head.size
  val ymax = input.size

  def addNeighbours(bat: Bat, bats: List[List[Bat]]): Unit = {
    val (i, j) = bat.pos
    for {
      y <- (i - 1) to (i + 1)
      x <- (j - 1 ) to (j + 1)
      if (y < ymax && x < xmax && x >= 0 && y >= 0 && (y, x) != bat.pos)
    } yield {
      bat.addPeer(bats(y)(x))
    }
    
  }

  bats.flatten.foreach(addNeighbours(_, bats))

  // logic
  def executeSteps(i: Int, bats: List[List[Bat]]): Unit = {
    def loop(bats: List[Bat]): Unit = {
      bats.foreach(_.tick())
      while (bats.find(_.shouldExplode).isDefined) {
        bats.foreach(_.explode())
      }
    } 

    for {
      c <- 1 to i
    } loop(bats.flatten)
  }


  // Part 1
  // executeSteps(100, bats )

  val result = bats.flatten.map(_.timesExploded).sum

  def triggerAll(i: Int, bats: List[List[Bat]]): Unit = {
    def loop(bats: List[Bat]): Unit = {
      if (bats.forall(_.value == 0)) {
        println("Iteration " + bats.head.iteration)
        throw new Exception(s"${bats.head.iteration}")
      }
      bats.foreach(_.tick())

      while (bats.find(_.shouldExplode).isDefined) {
        bats.foreach(_.explode())
      }
    } 

    for {
      c <- 1 to i
    } loop(bats.flatten)
  }

  triggerAll(300, bats)

  @main
  def main11 =
    bats.foreach(l => 
      l.foreach(b => 
        print(b.value))
      println("")
    )

    println(result)
