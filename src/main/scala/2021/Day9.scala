package `2021`

import scala.io.Source
import scala.util.{Try, Success, Failure}

object Day9:
  val input = Source.fromResource("2021/day9.txt").getLines().toList

  val parsed : Map[Int, List[Int]]= (for {
    (line, idx) <- input.zipWithIndex
    height <- line.map(c => Integer.parseInt(c.toString)).toList
  } yield {
    (idx -> height)
  }).groupBy((i, h) => i).map((k, v) => k -> v.map(_._2))
  case class Point(i: Int, coord: (Int, Int), neighbours: List[Int]) {
    def isMinimum: Boolean = neighbours.forall(_ > i)
  }


  def intToPoint(in: Map[Int, List[Int]]): List[Point] = {
    for {
      i <- (0 until in.size)
      j <- (0 until in.head._2.size)
    } yield {
      val height = in(i)(j)
      val left = Try(in(i)(j-1)).getOrElse(9)
      val right = Try(in(i)(j+1)).getOrElse(9)
      val up = Try(in(i-1)(j)).getOrElse(9)
      val down = Try(in.apply(i+1)(j)).getOrElse(9)
      Point(height, i -> j, List(left, right, up, down))
    }
  }.toList

  // def createPoint(coord: (Int, Int), in: Map[Int, List[Int]]): List[Point] = {
  //   val (i, j) = coord
  //   val height = in(i)(j)
  //   val left = Try(in(i)(j-1)).getOrElse(9)
  //   val right = Try(in(i)(j+1)).getOrElse(9)
  //   val up = Try(in(i-1)(j)).getOrElse(9)
  //   val down = Try(in.apply(i+1)(j)).getOrElse(9)
  //   List(left, right, up, down).
  //   val p = Point(height, i -> j, List(left, right, up, down))

  // }



  val minima: List[Point] = intToPoint(parsed).filter(_.isMinimum)

  val result = minima.map(_.i + 1).sum

  def canVisit(in: Map[Int, List[Int]], coord: (Int, Int)): Boolean = 
    in.get(coord._1) match 
      case Some(row) => 
        Try(row(coord._2)) match {
        case Success(height) => height < 9
        case _ => false
      }
      case None => 
        false

  // def walk(points: Map[Int, Map[Int, Boolean]], point: Point, count: Int): List[Int] = 
  //   val row = points(point.coord._1)
  //   val visited = row(point.coord._2)
  //   if (visited)
  //     walk(points, (coord._1, ))

  def exploreIsland(in:  Map[Int, List[Int]])(current: (Int, Int), todo: Set[(Int, Int)], visited: Set[(Int, Int)]): Int = {
    val (i, j) = current
    // println("CURRENT: " + current)
    val bag = visited + current
    // println("BAG " + bag)
    val addTodo = Set(
      (i, j - 1), 
      (i, j + 1), 
      (i - 1, j),
      (i + 1, j)
    ).filter(canVisit(in, _))
    // println("ADDTODO " + addTodo)
    val newTodo = (todo ++ addTodo).filterNot(bag.contains(_))
    // println("new todo " + newTodo)
    if (newTodo.isEmpty) {
      bag.size 
    } else {
      exploreIsland(in)(newTodo.head, newTodo.tail, bag)
    }
  }

  // val result2 = minima.map(point => )

  val result2 = minima.map(minimum => exploreIsland(parsed)(minimum.coord, Set(), Set())).toList.sortWith(_ > _).take(3).product

  @main
  def main9 =
    // println(result)
    println(result2)
