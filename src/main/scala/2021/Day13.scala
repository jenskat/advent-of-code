package `2021`

import scala.io.Source

object Day13:
  val input = Source.fromResource("2021/day13.txt").getLines().toList

  val points: Set[(Int, Int)] = input.takeWhile(_.nonEmpty).map(line => {
    val xy = line.split(',')
    xy(0).toInt -> xy(1).toInt
    }).toSet

  val instructions = List('x' -> 655,
    'y' -> 447,
    'x' -> 327,
    'y' -> 223,
    'x' -> 163,
    'y' -> 111,
    'x' -> 81,
    'y' -> 55,
    'x' -> 40,
    'y' -> 27,
    'y' -> 13,
    'y' -> 6)

  def fold(in: Set[(Int, Int)], action: (Char, Int)): Set[(Int, Int)] = 
    action match 
      case ('x', i) => in.map((x,y) => (helper(x, i), y))
      case ('y', j) => in.map((x,y) => (x, helper(y, j)))
  
  def helper(in: Int, minus: Int): Int =
    if (minus > in) in 
    else in - (2*(in - minus))

  val result = fold(points, instructions.head).size
  val result2: Set[(Int, Int)] = instructions.foldLeft(points)((acc, elem) => fold(acc, elem))

  @main
  def main13 =
    // println(points)
    println(result)
    println(result2)

    for (y <- 0 to 5) {
      for (x <- 0 to 40) {
        if (result2.contains((x,y))) print('#') else print('.')
      } 
      println()
    }
